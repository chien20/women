import axios from "axios";
import React, {useState, useEffect} from "react";
import { useParams } from "react-router";
import './App.css';

function App() {
  const [data, setData] = useState({});

  const { id } = useParams();

  useEffect(() => {
    axios.get(`http://103.9.159.151:22298/api/v1/womens/get-by-id/${id}`)
          .then(function (response) {
            setData(response?.data?.dances);
          })
          .catch(function (error) {
              console.log(error);
          });
  }, [id]);

  const done = () => {
    const body = {
      ...data,
      status: 1
    };
    axios.put(`http://103.9.159.151:22298/api/v1/womens/${id}`, body)
          .then(function (response) {
          })
          .catch(function (error) {
              console.log(error);
          });
    setData({...data, status: 1});
  }

  const busy = () => {
    const body = {
      ...data,
      status: 2
    };
    axios.put(`http://103.9.159.151:22298/api/v1/womens/${id}`, body)
          .then(function (response) {
          })
          .catch(function (error) {
              console.log(error);
          });
    setData({...data, status: 2});
  }


  return (
    <div className="App">
      <h4 className="title">Invitation for <span>{data?.name}</span></h4>
      <img className="img-main" src={process.env.PUBLIC_URL + "/women.png"} alt="women"/>
      {
        data.status === 0 &&
        <div className="button">
          <button onClick={done}>Of Course !</button>
          <button onClick={busy}>No. So busy !</button>
        </div>
      }
      {
        data.status === 1 &&
        <div className="button">
          <p className="p_done">Of Course !</p>
        </div>
      }
      {
        data.status === 2 &&
        <div className="button">
          <p className="p_busy">No. So busy !</p>
        </div>
      }
      <img className="avt" src={process.env.PUBLIC_URL + `/img/${id}.jpg`} alt="love"/>
    </div>
  );
}

export default App;
