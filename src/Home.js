import axios from "axios";
import React, {useState, useEffect} from "react";
import './App.css';

function App() {
  const [done, setDone] = useState([]);
  const [busy, setBusy] = useState([]);
  const [pending, setPending] = useState([]);

  useEffect(() => {
    axios.get('http://103.9.159.151:22298/api/v1/womens/get-all')
          .then(function (response) {
            const data = response?.data?.data
            console.log(data);
            const doneData = data.filter(item => item.status === 1);
            const busyData = data.filter(item => item.status === 2);
            const pendingData = data.filter(item => item.status === 0);
            setDone(doneData);
            setBusy(busyData);
            setPending(pendingData);
          })
          .catch(function (error) {
              console.log(error);
          });
  }, []);


  return (
    <div className="App home">
      {
          done.length > 0 &&
          <h3>- Will Participate -</h3>
      } 
      <div className="list">
        {
            done.map(item => (
                <div className="list-item" key={item.id}>
                    <img src={process.env.PUBLIC_URL + `/img/${item.id}.jpg`} alt="love"/>
                    <span>{item.name}</span>
                </div>
            )) 
        }
      </div>
      {
          busy.length > 0 &&
          <h3>- Busy Can't Join -</h3> 
      }
      <div className="list">
        {
            busy.map(item => (
                <div className="list-item" key={item.id}>
                    <img src={process.env.PUBLIC_URL + `/img/${item.id}.jpg`} alt="love"/>
                    <span>{item.name}</span>
                </div>
            )) 
        }
      </div>
      {
          pending.length > 0 &&
          <h3>- Waiting -</h3>
      } 
      <div className="list">
        {
            pending.map(item => (
                <div className="list-item" key={item.id}>
                    <img src={process.env.PUBLIC_URL + `/img/${item.id}.jpg`} alt="love"/>
                    <span>{item.name}</span>
                </div>
            )) 
        }
      </div>
    </div>
  );
}

export default App;
