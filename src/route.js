import React, { lazy, Suspense } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

const App = lazy(() => import('./App'));
const Home = lazy(() => import('./Home'));


const RouterApp = () => {
  return (
    <Router>
      <Suspense fallback={<></>}>
          <Switch>
            <Route path="/:id" exact>
                <App/>
            </Route>
            <Route path="/" exact>
                <Home/>
            </Route>
            <Redirect to="/"></Redirect>
          </Switch>
        </Suspense>
    </Router>
  )
}
export default React.memo(RouterApp);